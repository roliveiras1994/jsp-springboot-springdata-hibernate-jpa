<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<title>Lista de Cartorios Cadastrados</title>
</head>
<body>
	<h2>Lista de Cartorios Cadastrados</h2>


	<c:if test="${not empty cartorios}">
		<table>
			<tr>
				<td>Nome</td>

			</tr>

			<c:forEach var="listValue" items="${cartorios}">

				<tr>
					<td>${listValue.nome}</td>
					<td><a href="/cartorio/edit?id=${listValue.id}">Editar</a></td>
					<td><a href="/cartorio/delete?id=${listValue.id}">Excluir</a></td>
					<td><a href="/cartorio/details?id=${listValue.id}">Detalhes</a></td>
				</tr>
			</c:forEach>
			</c:if>

		</table>

		<a href="/cartorio/add">Adicionar</a>
</body>
</html>