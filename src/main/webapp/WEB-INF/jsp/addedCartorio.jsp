<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
<title>Cartorio Cadastrado</title>
</head>
<body>
	<h2>Cartorio Adicionado</h2>


	<c:if test="${not empty cartorio}">
		<table>
			<tr>
				<td>ID</td>
				<td>Nome</td>

			</tr>

			<tr>
				<td>${cartorio.id}</td>
				<td>${cartorio.nome}</td>
				
			</tr>
			</c:if>

		</table>

		<a href="/cartorio/list">Lista</a>
		<a href="/cartorio/add">Adicionar</a>
</body>
</html>