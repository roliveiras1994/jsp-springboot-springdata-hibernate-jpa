package br.com.cartorio.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "CARTORIO")
public class CartorioEntity {

	@GeneratedValue
	@Id
	private long Id;

	@Column(nullable = false, length = 128, unique = true)
	private String nome;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
