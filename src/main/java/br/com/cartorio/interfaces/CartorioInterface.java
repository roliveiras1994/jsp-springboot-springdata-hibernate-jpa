package br.com.cartorio.interfaces;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.cartorio.entities.CartorioEntity;

@RepositoryRestResource(path = "/cartorio")
public interface CartorioInterface extends CrudRepository<CartorioEntity, Long> {

}
