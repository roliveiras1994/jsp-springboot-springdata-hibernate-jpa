package br.com.cartorio.controllers;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.cartorio.entities.CartorioEntity;
import br.com.cartorio.interfaces.CartorioInterface;

@Controller
public class CartorioController {

	@Autowired
	private CartorioInterface cartorioInterface;

	@RequestMapping(path = "/cartorio/list", method = RequestMethod.GET)
	public String listCartorio(Map<String, Object> model) {

		List<CartorioEntity> cartorios = (List<CartorioEntity>) cartorioInterface.findAll();
		model.put("cartorios", cartorios);
		return "listCartorio";

	}

	@RequestMapping(path = "/cartorio/add", method = RequestMethod.GET)
	public ModelAndView viewCreateCartorio() {

		return new ModelAndView("addCartorio", "command", new CartorioEntity());

	}

	@RequestMapping(path = "/cartorio/add", method = RequestMethod.POST)
	public String CreateCartorio(Map<String, Object> model, CartorioEntity entity) {

		cartorioInterface.save(entity);

		model.put("cartorio", entity);

		return "addedCartorio";

	}

	@RequestMapping(path = "/cartorio/delete", method = RequestMethod.GET)
	public String deleteCartorio(@RequestParam Long id) {

		CartorioEntity entity = cartorioInterface.findById(id).get();

		cartorioInterface.delete(entity);

		return "redirect:/cartorio/list";

	}

	@RequestMapping(path = "/cartorio/details", method = RequestMethod.GET)
	public String viewCartorio(@RequestParam long id, Map<String, Object> model) {

		CartorioEntity entity = cartorioInterface.findById(id).get();

		model.put("cartorio", entity);

		return "addedCartorio";
	}

	@RequestMapping(path = "/cartorio/edit", method = RequestMethod.GET)
	public ModelAndView viewEditCartorio(@RequestParam long id) {

		CartorioEntity entity = cartorioInterface.findById(id).get();

		return new ModelAndView("editCartorio", "command", entity);

	}

	@RequestMapping(path = "/cartorio/edit", method = RequestMethod.POST)
	public String editCartorio(Map<String, Object> model, CartorioEntity entity) {

		cartorioInterface.save(entity);

		model.put("cartorio", entity);

		return "redirect:/cartorio/list";

	}

}
